% Fonction permettant d'afficher les images avec les points d'interet
% detectes
% ENTREES
% Im : Image
% points : les points à afficher
% message : le titre de la figure
% l, c, numero : le nombre de lignes et colonnes de la figure et le numero
% dans la sous-figure
% SORTIE : la figure
function [] = affichage_POI(Im,points,message,l,c,numero)
% Affichage de l'image
subplot(l,c,numero);
colormap(gray);imshow(Im); hold on;
plotpoints(points,length(points),'r+'); 
hold off; 
end
