function [ Imos ] = mosaique3( I1, I2, I3, H21, H23 )
% On recupere la taille des deux images. 
[nblI1 nbcI1 nb_canaux] = size(I1);
[nblI2 nbcI2 nb_canaux] = size(I2);
[nblI3 nbcI3 nb_canaux] = size(I3);


% On calcule l'homographie inverse, normalisee, 
% pour nous permettre d'effectuer la transformation de I2 vers I1. 
Hinv21=inv(H21);
Hinv21=Hinv21./Hinv21(3,3);

% On calcule l'homographie inverse, normalisee, 
% pour nous permettre d'effectuer la transformation de I2 vers I1. 
Hinv23=inv(H23);
Hinv23=Hinv23./Hinv23(3,3);


% Calcul des dimensions de la mosaique. 
% Calcul des quatre coins dans l'image 1 et 3. 
% les coins de l'image 1 et 3 sont ranges en ligne selon : 
% haut_gauche, haut_droit, bas_droit, bas gauche. 
% Lignes et colonnes sont inversees.  
xy_coinsI1_R1 = [1 1; nbcI1 1; nbcI1 nblI1; 1 nblI1];
xy_coinsI3_R3 = [1 1; nbcI3 1; nbcI3 nblI3; 1 nblI3];

% Application de l'homographie Hinv sur ces coins. 
% Calcul des images des coins dans I2. 
xy_coinsI1_R2 = appliquerHomographie(Hinv21,xy_coinsI1_R1)
xy_coinsI3_R2 = appliquerHomographie(Hinv23,xy_coinsI3_R3)

% Determination des dimensions de l'image mosaique, 
% les xmin ymin xmax ymax, ou :
%  - xmin represente la plus petite abscisse parmi les abscisses des images 
%    des coins de I1 et I3 projetes dans I2 et les coins dans I2, 
%  - etc
% Lignes et colonnes sont inversees. 

xmin=min([xy_coinsI1_R2(:,1)' 1]);
ymin=min([xy_coinsI1_R2(:,2)' 1 xy_coinsI3_R2(:,2)']);
xmax=max([xy_coinsI3_R2(:,1)' nbcI2]);
ymax=max([xy_coinsI1_R2(:,2)' nblI2 xy_coinsI3_R2(:,2)']);

% On arrondit de maniere a etre certain d'avoir les coordonnees initiales
% bien comprises dans l'image. 
xmin=floor(xmin);
ymin=floor(ymin);
xmax=ceil(xmax);
ymax=ceil(ymax);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CONTRUCTION DE LA MOSAIQUE %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calcul de la taille de la mosaique. 
% Lignes et colonnes sont inversees. 
nblImos=ymax-ymin+1;
nbcImos=xmax-xmin+1;

Imos=zeros(nblImos,nbcImos, nb_canaux);

% Calcul de l'origine de l'image I2 dans le repere de la mosaique Imos. 
O2x_Rmos = 1-(xmin-1);
O2y_Rmos = 1-(ymin-1);


% Copie de l'image I2. 
% Lignes et colonnes sont inversees. 
Imos(O2y_Rmos:O2y_Rmos+nblI2-1, O2x_Rmos:O2x_Rmos+nbcI2-1, :) = I2;

% Copie de l'image I1 et I3 transformees par les homographies. 
for x=1:nbcImos,
  for y=1:nblImos,
    % Calcul des coordonnees dans I2 connaissant les coordonnees du point origine de I2 dans Imos. 
    y_R2=y-O2y_Rmos+1;
    x_R2=x-O2x_Rmos+1;
    % Dans le repere attache a l'image I2, 
    % nous estimons les coordonnees du point image de (y_R2,x_R2)
    % par l'homographie H21 : (xy_R1). 
    %idem I3.
    xy_R1 = appliquerHomographie(H21,[x_R2 y_R2]);
    xy_R3 = appliquerHomographie(H23,[x_R2 y_R2]);
    
    % Il existe plusieurs strategies, mais, ici, 
    % pour estimer les coordonnees (entieres) , 
    % on choisit : sans interpolation, le plus proche voisin. 
    x_R1=round(xy_R1(1)); 
    y_R1=round(xy_R1(2));

    x_R3=round(xy_R3(1)); 
    y_R3=round(xy_R3(2));
    
    % On verifie que xy_R1 appartient bien a l'image I1
    % avant d'affecter cette valeur a Imos
    % Lignes et colonnes sont inversees.
    if(x_R1>=1 & x_R1<=nbcI1 & y_R1>=1 & y_R1<=nblI1)
        if (all (Imos(y,x, :) == zeros(1,1,3)))
            Imos(y,x, :)=I1(y_R1,x_R1, :);
        else
            d1= nbcI1-x_R1;
            d2 = x_R2;
            p1 = d1 / (d1+d2);
            p2 = d2 / (d1+d2);
            if(x_R2>=1 & x_R2<=nbcI2 & y_R2>=1 & y_R2<=nblI2)
                Imos(y,x,:)= p1 * I1(y_R1, x_R1,:) + p2 * I2(y_R2, x_R2,:);
            else
                Imos(y,x,:)=I1(y_R1,x_R1,:);
            end
        end
            
    end
    
    if(x_R3>=1 & x_R3<=nbcI3 & y_R3>=1 & y_R3<=nblI3)
        if (all (Imos(y,x, :) == zeros(1,1,3)))
            Imos(y,x, :)=I3(y_R3,x_R3, :);
        else
            d2= nbcI2-x_R2;
            d3 = x_R3;
            p2 = d2 / (d2+d3);
            p3 = d3 / (d2+d3);
            if(x_R2>=1 & x_R2<=nbcI2 & y_R2>=1 & y_R2<=nblI2)
                Imos(y,x,:)= p3 * I3(y_R3, x_R3,:) + p2 * I2(y_R2, x_R2,:);
            else
                Imos(y,x,:)=I3(y_R3,x_R3,:);
            end
        end
            
    end
    
    
    

  end
end