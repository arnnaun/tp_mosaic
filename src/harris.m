function [XY,Res]=harris(Im,TailleFenetre,NbPoints,k);
%[I,J,Res]=harris(Im,TailleFenetre,NbPoints,k);
%
% Im -> image
% TailleFenetre -> taille (impaire) de la fenetre du voisinage
%                  valeurs conseillees : entre 9 et 25
% NbPoints -> nombre de points desires
% k -> poids utilise dans le calcul de la reponse R, k dans [0.04,0.06]
% XY -> Matrice de taille NbPointsx2 contenant les coordonnees des points d interets extraits
% Res -> Image des reponses par le detecteur de Harris

% Verification et correction eventuelle des parametres donnes
if nargin < 4, k=0.05;      end;
if nargin < 3, NbPoints=50;      end;
if nargin < 2, TailleFenetre=9;  end;

% Calcul des filtres utilises pour le calcul de la reponse du detecteur de
% Harris (lissage, derivation suivant les lignes, derivation suivant les
% colonnes)
[L,Di,Dj]=gaussmask(TailleFenetre);

% Convolution image originale pour obtenir les images des derivees
% premieres partielles
% Utiliser conv2 avec l'option 'same'
 Im = double(Im);
%Im = double(edge(Im, 'canny', 0.2));
Ii=conv2(Im,Di,'same');
Ij=conv2(Im,Dj,'same');

% Calcul des elements A, B et C
A1=Ii.^2;
B1=Ij.^2;
C1=Ii.*Ij;
A=conv2(A1,L,'same');
B=conv2(B1,L,'same');
C=conv2(C1,L,'same');

% (1) Calcul de la reponse suivant l'equation (1)
R=A.*B-C.^2-k*(A+B).^2;

% (2) Suppression des non-maxima locaux suivant l'equation (2)
n2=floor(TailleFenetre/2);
[l,c]=size(Im);
Res=zeros(l,c);
for i=(n2+1):(l-n2)
  for j=(n2+1):(c-n2)
    if R(i,j)==max(max(R(i-n2:i+n2,j-n2:j+n2)))
      Res(i,j)=R(i,j);
    end;
  end;
end;

% (3) Selection des NbPoints en fonction de "NbPoints" reponses les plus fortes
% Tri des reponses : utiliser sort
[s,is]=sort(Res(:),'descend');

% Selection des indices de NbPoints de plus fortes reponses
is=is(1:NbPoints);

% Calcul des indices dans l'image 
% Utiliser ind2sub
[I,J]= ind2sub(size(Res),is);

XY = [J I];