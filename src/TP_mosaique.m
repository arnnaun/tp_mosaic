clear all;
close all;
% Lecture et affichage des images
Im1 = imread('image1.pgm');
Im2 = imread('image2.pgm');
Im3 = imread('image3.pgm');
figure;
affichage_image(Im1,'Image 1',1,3,1);
affichage_image(Im2,'Image 2',1,3,2);
affichage_image(Im3,'Image 3',1,3,3);

% Choix des parametres
TailleFenetre = 9;
NbPoints = 100 ; 
k = 0.05;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Detection des points d'interet avec Harris %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% A DECOMMENTER POUR OBSERVER LA DETECTION DE HARRIS
[XY_1,Res_1] = harris(Im1,TailleFenetre,NbPoints,k);
[XY_2,Res_2] = harris(Im2,TailleFenetre,NbPoints,k);
[XY_3,Res_3] = harris(Im3,TailleFenetre,NbPoints,k);
figure;
affichage_POI(Im1,XY_1,'POI Image 1',1,3,1);
affichage_POI(Im2,XY_2,'POI Image 2',1,3,2);
affichage_POI(Im3,XY_3,'POI Image 3',1,3,3);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Appariement des points d'interet %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% A DECOMMENTER POUR OBSERVER LA MISE EN CORRESPONDANCE
[XY_C1,XY_C21] = apparier_POI(Im1,XY_1,Im2,XY_2,TailleFenetre,0.9);
[XY_C23,XY_C3] = apparier_POI(Im2,XY_2,Im3,XY_3,TailleFenetre,0.9);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Estimation (et verification) de l'homographie %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% A DECOMMENTER QUAND HOMOGRAPHIE AURA ETE COMPLETEE
H21 = homographie(XY_C21,XY_C1);
H23 = homographie(XY_C23,XY_C3);

%%%%%%%%%%%%%%%%%%%%%%%%%
% Calcul de la mosaique %
%%%%%%%%%%%%%%%%%%%%%%%%%
% A DECOMMENTER QUAND HOMOGRAPHIE AURA ETE VALIDEE
%Imos = mosaique(Im1,Im2,H);
%figure; 
%affichage_image(uint8(Imos),'Mosaique obtenue a partir des 2 images initiales',1,1,1);

% Version 2 pour la reconstruction
% A DECOMMENTER QUAND MOSAIQUEBIS AURA ETE ECRITE
% Imosbis = mosaiquebis(Im1,Im2,H);
% figure; 
% affichage_image(uint8(Imosbis),'Mosaique obtenue a partir des 2 images initiales (version 2)',1,1,1);

% Version 3 pour la reconstruction avec les couleurs R, G et B
% A DECOMMENTER QUAND MOSAIQUECOUL AURA ETE ECRITE
% Im1_coul = imread('image_couleur1.jpg');
% Im2_coul = imread('image_couleur2.jpg');
% Imoscoul = mosaiquecoul(Im1_coul,Im2_coul,H);
% figure;
% affichage_image(uint8(Imoscoul),'Mosaique obtenue a partir des 2 images couleur initiales (version 2)',1,1,1);

% Version 4 pour la reconstruction avec plus de deux images
% A COMPLETER ...
Im1_coul = imread('image_couleur1.jpg');
Im2_coul = imread('image_couleur2.jpg');
Im3_coul = imread('image_couleur3.jpg');
Imoscoul3 = mosaique3(Im1_coul,Im2_coul,Im3_coul,H21, H23);
figure;
affichage_image(uint8(Imoscoul3),'Mosaique obtenue a partir des 3 images couleur initiales (version X)',1,1,1);