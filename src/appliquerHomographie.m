% Calcul des coordonnees (xy2) des points (xy1)
% apres application d'une homographique H

function [xy2] = appliquerHomographie(H,xy1)

% Entrees :
%
% H   : matrice (3x3) de l'homographie
% xy1 :  matrice (nbPoints x 2) representant les coordonnees 
%       (colonne 1 : les x, colonne 2 : les y) 
%       des nbPoints points auxquels H est appliquee
%
% Sortie :
% xy2 : coordonnees des points apres application de l'homographie

% Nombre de points
% ... A completer ...
[nb_points,~] = size(xy1);

% Construction des coordonnees homogenes pour appliquer l'homographie
% ... A completer ...
xy1 = [xy1';ones(1,nb_points)];

% Application de l'homographie
% ... A completer ...
xy2 = H*xy1;

% On retourne les coordonnees homogenes (x,y,1)
% Pour cela, il faut diviser par z
% ... A completer ...
xy2 = (xy2./repmat(xy2(3,:),3,1))';
xy2 = xy2(:,1:2);