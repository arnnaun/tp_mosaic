# Construction d'une mosaïque à partir de plusieurs images

Le but de ces deux séances de travaux pratiques est de réaliser un programme, en matlab, permettant
de créer une mosaïque d’images à partir de plusieurs (deux voire plus) images qui possèdent une zone de
recouvrement ou zone commune. On suppose que l’alignement géométrique des images peut s’effectuer
selon une homographie. Cette hypothèse peut être faite lorsque les images sont prises avec une caméra
à laquelle on a fait subir une rotation autour de son centre optique. La démarche proposée est la
suivante :

1. Détecter des points d’intérêt avec le détecteur de Harris, étudié et implémenté en 2A.
2. Mettre en correspondance ces points par l’approche par corrélation, étudié et implémenté en 2A.
3. Estimer l’homographie à partir des correspondances obtenues en (2), à faire pour ce TP.
4. Construire une mosaïque en utilisant l’homographie estimée en (3), à faire pour ce TP.
